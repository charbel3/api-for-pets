<?php

namespace Utility;

use PDO;

class Connection extends PDO
{
  public function __construct()
  {
    $process = file_get_contents('./process.json');
    $config = json_decode($process);

    parent::__construct("mysql:host={$config->host};dbname={$config->dbname};",
      $config->username,
      $config->password,
      array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $this->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
  }
}