<?php
  function generateError($code, $message){
      $err = [
          'status' => $code,
          'message' => $message
      ];
      header("HTTP/1.0 $code $message");

      return $err;
  }