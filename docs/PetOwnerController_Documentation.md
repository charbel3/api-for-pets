# PetOwnerController Class

The `PetOwnerController` class is responsible for handling requests related to pet owners. It interacts with the `PetOwnerModel` to perform CRUD (Create, Read, Update, Delete) operations on pet owner data. This controller provides endpoints for retrieving pet owners, adding new pet owners, and deleting existing pet owners.

## Constructor

### `__construct()`

Initializes a new instance of the `PetOwnerController` class and sets the `method` property.

## Methods

### `index()`

Handles the `GET` request for the `/pet-owners` endpoint.

- **Method**: GET
- **Endpoint**: `/pet-owners`

#### Example Usage

```php
$petOwnerController->index();
```

#### Success Response

- **Code**: 200
- **Content**: JSON array of pet owners' data

#### Error Response

- **Code**: 404
- **Content**: JSON error response indicating no pet owners found

- **Code**: 405
- **Content**: JSON error response indicating the method is not allowed

### `show()`

Handles the `GET` request for the `/pet-owners` endpoint if the `id` is set.

- **Method**: GET
- **Endpoint**: `/pet-owners`

#### Example Usage

```php
$petOwnerController->show();
```

#### Success Response

- **Code**: 200
- **Content**: JSON object with the pet owner's data

#### Error Response

- **Code**: 404
- **Content**: JSON error response indicating no pet owner found

### `add()`

Handles the `POST` request for the `/pet-owners/add` endpoint.

- **Method**: POST
- **Endpoint**: `/pet-owners/add`

#### Example Usage

```php
$petOwnerController->add();
```

#### Success Response

- **Code**: 200
- **Content**: JSON object with the added pet owner's data

#### Error Response

- **Code**: 422
- **Content**: JSON error response indicating missing required parameters

- **Code**: 405
- **Content**: JSON error response indicating the method is not allowed

### `delete()`

Handles the `DELETE` request for the `/pet-owner/delete` endpoint.

- **Method**: DELETE
- **Endpoint**: `/pet-owner/delete`

#### Example Usage

```php
$petOwnerController->delete();
```

#### Success Response

- **Code**: 200
- **Content**: JSON object with the deleted pet owner's data

#### Error Response

- **Code**: 404
- **Content**: JSON error response indicating no pet owner found

- **Code**: 422
- **Content**: JSON error response indicating missing ID parameter

- **Code**: 405
- **Content**: JSON error response indicating the method is not allowed