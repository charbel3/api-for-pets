# PetController Class

The `PetController` class is responsible for handling requests related to pets. It interacts with the `PetModel` to perform CRUD (Create, Read, Update, Delete) operations on pet data. This controller provides endpoints for retrieving pets, adding new pets, deleting existing pets, and editing pet information.

## Constructor

### `__construct()`

Initializes a new instance of the `PetController` class and sets the `method` property.

## Methods

### `index()`

Handles the `GET` request for the `/pets` endpoint.

- **Method**: GET
- **Endpoint**: `/pets`

#### Example Usage

```php
$petController->index();
```

#### Success Response

- **Code**: 200
- **Content**: JSON array of pets' data

#### Error Response

- **Code**: 404
- **Content**: JSON error response indicating no pets found

- **Code**: 405
- **Content**: JSON error response indicating the method is not allowed

### `show()`

Handles the `GET` request for the `/pets` endpoint if the `id` is set.

- **Method**: GET
- **Endpoint**: `/pets`

#### Example Usage

```php
$petController->show();
```

#### Success Response

- **Code**: 200
- **Content**: JSON object with the pet's data

#### Error Response

- **Code**: 404
- **Content**: JSON error response indicating no pet found

### `add()`

Handles the `POST` request for the `/pets/add` endpoint.

- **Method**: POST
- **Endpoint**: `/pets/add`

#### Example Usage

```php
$petController->add();
```

#### Success Response

- **Code**: 200
- **Content**: JSON object with the added pet's data

#### Error Response

- **Code**: 422
- **Content**: JSON error response indicating missing required parameters

- **Code**: 405
- **Content**: JSON error response indicating the method is not allowed

### `delete()`

Handles the `DELETE` request for the `/pets/delete` endpoint.

- **Method**: DELETE
- **Endpoint**: `/pets/delete`

#### Example Usage

```php
$petController->delete();
```

#### Success Response

- **Code**: 200
- **Content**: JSON object with the deleted pet's data

#### Error Response

- **Code**: 404
- **Content**: JSON error response indicating no pet found

- **Code**: 422
- **Content**: JSON error response indicating missing ID parameter

- **Code**: 405
- **Content**: JSON error response indicating the method is not allowed

### `edit()`

Handles the `PUT` request for the `/pets/edit` endpoint.

- **Method**: PUT
- **Endpoint**: `/pets/edit`

#### Example Usage

```php
$petController->edit();
```

#### Success Response

- **Code**: 200
- **Content**: JSON object with the edited pet's data

#### Error Response

- **Code**: 404
- **Content**: JSON error response indicating no pet found

- **Code**: 422
- **Content**: JSON error response indicating missing required parameters

- **Code**: 405
- **Content**: JSON error response indicating the method is not allowed