<?php

require_once 'autoload.php';

$path = isset($_GET['path']) ? $_GET['path'] : '/';

$routes = array(
  '/pets' => ['PetController', 'index'],
  '/pets/add' => ['PetController', 'add'],
  '/pets/delete' => ['PetController', 'delete'],
  '/pets/edit' => ['PetController', 'edit'],

  '/pet-owners' => ['PetOwnerController', 'index'],
  '/pet-owners/add' => ['PetOwnerController', 'add'],
  '/pet-owners/delete' => ['PetOwnerController', 'delete']
);

if (isset($routes[$path])) {
  list($controller, $action) = $routes[$path];

  require_once 'controllers/'. $controller . '.php';

  $petController = new ('\controllers\\' . $controller)();
  $petController->$action();
}