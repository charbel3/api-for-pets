<?php

require_once('utility/Connection.php');
require_once('utility/globals.php');

require_once('models/Pet.php');
require_once('models/PetOwner.php');
require_once('classes/Pet.php');
require_once('classes/PetOwner.php');
