# API-for-Pets

This repository contains an api for managing pets and pet owners.

## Installation

1. Clone the repository to your local machine.
2. Make sure you have PHP installed on your system.
3. Set up a web server (e.g., Apache, Nginx) and configure it to serve the repository's files.
4. Ensure that the server has proper PHP configuration and extensions enabled.

## Usage

### PetController

The `PetController` handles requests related to pets. It provides the following endpoints:

- `/pets`: Retrieves all pets.
- `/pets?id={id}`: Retrieves the pet with the given `{id}`.
- `/pets/add`: Adds a new pet.
- `/pets/delete`: Deletes an existing pet.
- `/pets/edit`: Edits an existing pet.

For detailed documentation on how to use the `PetController` endpoints, refer to the [PetController Documentation](docs/PetController_Documentation.md).

### PetOwnerController

The `PetOwnerController` handles requests related to pet owners. It provides the following endpoints:

- `/pet-owners`: Retrieves all pet owners.
- `/pet-owners?id={id}`: Retrieves the pet owner with the given `{id}`.
- `/pet-owners/add`: Adds a new pet owner.
- `/pet-owners/delete`: Deletes an existing pet owner.

For detailed documentation on how to use the `PetOwnerController` endpoints, refer to the [PetOwnerController Documentation](docs/PetOwnerController_Documentation.md).

## File Structure

- `classes/`: Directory containing the classes for pets and pet owners.
- `controllers/`: Directory containing controller classes.
- `docs/`: Contains Documentations.
- `models/`: Directory containing model classes.
- `utility/`: Directory containing the database connection and the global functions.
- `views/`: Directory containing view files.
- `.htaccess`: File containing rewrite rules.
- `autoload.php`: Autoloads classes.
- `index.php`: Entry point for handling requests and routing them to the appropriate controllers.
- `pets.sql`: Database structure for api and some sample data.
- `process.example.json`: Structure for process.json file.