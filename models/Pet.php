<?php

namespace Models;

Use Utility\Connection;
Use PDO;
Use Classes\Pet;

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');

class PetModel {

    private $db;

    function __construct() {
        $this->db = new Connection();
    }

    public function SelectAllPets() {        

        $data = $this->db->prepare("SELECT * FROM tbl_pets");
        $data->execute();

        $pets = array();

        while($pet = $data->fetch(PDO::FETCH_ASSOC)) {
            $info = Pet::fromArray($pet)->toArray();

            array_push($pets, $info);
        }

        return $pets;
    }

    public function SearchByID($id) {

        $data = $this->db->prepare("SELECT * FROM tbl_pets WHERE id = '$id' LIMIT 1");
        $data->execute();

        $pet = $data->fetch(PDO::FETCH_ASSOC);

        $output = Pet::fromArray($pet);

        return $output->toArray();
    }

    public function addPet($params) {

        $name        = $params['name'];
        $age         = $params['age'];
        $description = $params['description'];

        $data = $this->db->prepare("INSERT INTO tbl_pets (name, age, description) VALUES ('$name', '$age', '$description')");
        $data->execute();

        $id = $this->db->lastInsertId();

        $pet = Pet::fromParams( $id,
                                $name,
                                $age,
                                $description);

        return $pet->toArray();
    }

    public function deletePet($id) {

        $pet = $this->SearchByID($id);

        $data = $this->db->prepare("DELETE FROM tbl_pets WHERE id = '$id' LIMIT 1");
        $data->execute();

        return $data? Pet::fromArray($pet)->toArray() : array();
    }


    public function editPet($params, $input) {
        
        $id = $input['id'];

        $name        = $params['name'];
        $age         = $params['age'];
        $description = $params['description'];

        $data = $this->db->prepare("UPDATE tbl_pets SET name='$name', age='$age', description='$description' WHERE id = '$id' LIMIT 1");
        $data->execute();

        $pet = Pet::fromParams( $id,
                                $name,
                                $age,
                                $description);

        return $pet->toArray();
    }

}