<?php

namespace Models;

Use Utility\Connection;
Use PDO;
Use Classes\PetOwner;

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');

class PetOwnerModel {

    private $db;

    function __construct() {
        $this->db = new Connection();
    }

    public function SelectAllOwners() {        

        $data = $this->db->prepare("SELECT * FROM tbl_pet_owners");
        $data->execute();

        $owners = array();

        while($owner = $data->fetch(PDO::FETCH_ASSOC)) {
            $info = PetOwner::fromArray($owner)->toArray();

            array_push($owners, $info);
        }

        return $owners;
    }

    public function SearchByID($id) {

        $data = $this->db->prepare("SELECT * FROM tbl_pet_owners WHERE id = '$id' LIMIT 1");
        $data->execute();

        $owner = $data->fetch(PDO::FETCH_ASSOC);

        $output = PetOwner::fromArray($owner);

        return $output->toArray();
    }

    public function addOwner($params) {

        $name       = $params['name'];
        $email      = $params['email'];
        $password   = $params['password'];

        $hashed_password = password_hash($password, PASSWORD_DEFAULT);
        

        $data = $this->db->prepare("INSERT INTO tbl_pet_owners (name, email, password) VALUES ('$name', '$email', '$hashed_password')");
        $data->execute();

        $id = $this->db->lastInsertId();

        $owner = PetOwner::fromParams( $id,
                                        $name,
                                        $email,
                                        $hashed_password);

        return $owner->toArray();
    }

    public function deletePetOwner($id) {

        $owner = $this->SearchByID($id);

        $data = $this->db->prepare("DELETE FROM tbl_pet_owners WHERE id = '$id' LIMIT 1");
        $data->execute();

        return $data? PetOwner::fromArray($owner)->toArray() : array();
    }

}