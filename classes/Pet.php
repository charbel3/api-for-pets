<?php

namespace Classes;

class Pet {

    private $id;
    private $name;
    private $age;
    private $description;

    public static function fromParams($id, $name, $age, $description) {
        $pet = new Self;

        $pet->id            = $id;
        $pet->name          = $name;
        $pet->age           = $age;
        $pet->description   = $description;

        return $pet;
    }

    public static function fromArray($arr) {
        $pet = new Self;

        $pet->id            = $arr['id'];
        $pet->name          = $arr['name'];
        $pet->age           = $arr['age'];
        $pet->description   = $arr['description'];

        return $pet;
    }

    public function getId()             { return $this->id; }
    public function getName()           { return $this->name; }
    public function getAge()            { return $this->age; }
    public function getDescription()    { return $this->description; }

    public function setId($id)                      { $this->id = $id; return $this; }
    public function setName($name)                  { $this->name = $name; return $this; }
    public function setAge($age)                    { $this->age = $age; return $this; }
    public function setDescription($description)    { $this->description = $description; return $this; }

    public function toArray() {
        return array(
            'id'            => $this->id,
            'name'          => $this->name,
            'age'           => $this->age,
            'description'   => $this->description
        );
    }
}
?>