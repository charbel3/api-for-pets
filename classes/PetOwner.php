<?php

namespace Classes;

class PetOwner {

    private $id;
    private $name;
    private $email;
    private $password;
    private $status;

    public static function fromParams($id, $name, $email, $password) {
        $owner = new Self;

        $owner->id            = $id;
        $owner->name          = $name;
        $owner->email         = $email;
        $owner->password      = $password;
        $owner->status        = 1;

        return $owner;
    }

    public static function fromArray($arr) {
        $owner = new Self;

        $owner->id            = $arr['id'];
        $owner->name          = $arr['name'];
        $owner->email         = $arr['email'];
        $owner->password      = $arr['password'];
        $owner->status        = 1;

        return $owner;
    }

    public function getId()         { return $this->id; }
    public function getName()       { return $this->name; }
    public function getEmail()      { return $this->email; }
    public function getPassword()   { return $this->password; }
    public function getStatus()     { return $this->status; }

    public function setId($id)              { $this->id = $id;              return $this; }
    public function setName($name)          { $this->name = $name;          return $this; }
    public function setEmail($age)          { $this->email = $email;        return $this; }
    public function setPassword($password)  { $this->password = $password;  return $this; }
    public function setStatus($status)      { $this->status = $status;      return $this; }

    public function toArray() {
        return array(
            'id'            => $this->id,
            'name'          => $this->name,
            'email'         => $this->email,
            'status'        => $this->status
        );
    }
}
?>