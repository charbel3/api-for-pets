<?php

namespace Controllers;

use Models\PetModel;

class PetController
{

  private $method;

  public function __construct(){
    $this->method = $_SERVER['REQUEST_METHOD'];
  }

  public function index(){
    if($this->method == 'GET'){
      if(isset($_GET['id'])){
        $this->show();
      }else{
        $api = new PetModel();
        $pets = $api->SelectAllPets();

        if(empty($pets)){
          $err = generateError(404, "No Pets Found");
          echo json_encode($err);
        }else{
          echo json_encode($pets);
        }
      }
    }else{
      $err = generateError(405, $this->method . ' Method Not Allowed');
		  echo json_encode($err);
    }
  }
  public function show(){
    $id = $_GET['id'];

    $api = new PetModel();

    $pet = $api->SearchByID($id);

    if(empty($pet)) {
      $err = generateError(404, "No Pets Found");
      echo json_encode($err);
    }else{
      echo json_encode($pet);
    }
  }

  public function add(){
    if($this->method == 'POST'){

      $api = new PetModel();

      $inputData = json_decode(file_get_contents("php://input"), true);

      $params = empty($inputData)? $_POST: $inputData;

      $has_error = false;

      $required = array('name', 'age', 'description');

      foreach($required as $requirement){
        if(!isset($params[$requirement])){
          $has_error = true;
          $err = generateError(422,"Missing $requirement");
          echo json_encode($err);
          break;
        }
      }

      if(!$has_error){
        $pet = $api->addPet($params);
        echo json_encode($pet);
      }
    }else{
      $err = generateError(405, $this->method . ' Method Not Allowed');
		  echo json_encode($err);
    }
    
  }

  public function delete(){
    if($this->method == 'DELETE'){
      if (isset($_GET['id'])) {
        
        $api = new PetModel();
  
        $pet = $api->deletePet($_GET['id']);
  
        if(empty($pet)){
          $err = generateError(404, "No Pets Found");

          echo json_encode($err);
        }else{
          echo json_encode($pet);
        }
      }else{
        $err = generateError(422, "Missing ID");
        echo json_encode($err);
      }
    }else{
      $err = generateError(405, $this->method . " Method Not Allowed");
		  echo json_encode($err);
    }
  }

  public function edit(){
    if($this->method == 'PUT'){
      $api = new PetModel();

      $inputData = json_decode(file_get_contents("php://input"), true);

      $input = empty($inputData)? $_POST: $inputData;

      $has_error = false;

      $required = array('name', 'age', 'description');

      foreach($required as $requirement){
        if(!isset($params[$requirement])){
          $has_error = true;
          $err = generateError(422,"Missing $requirement");
          echo json_encode($err);
          break;
        }
      }

      if(!$has_error){
        $pet = $api->editPet($_GET, $input);

            if(empty($pet)) {
                $err = generateError(404, "No Pets Found");
                echo json_encode($err);
            }else{
                echo json_encode($pet);
            }
      }
    }else{
      $err = generateError(405, $this->method . ' Method Not Allowed');
		  echo json_encode($err);
    }
  }

}