<?php

namespace Controllers;

use Models\PetOwnerModel;

class PetOwnerController
{
  private $method;

  public function __construct(){
    $this->method = $_SERVER['REQUEST_METHOD'];
  }

  public function index(){
    if($this->method == 'GET'){
        if(isset($_GET['id'])){
          $this->show();
        }else{
          $api = new PetOwnerModel();
          $owners = $api->SelectAllOwners();
  
          if(empty($owners)){
            $err = generateError(404, "No Owners Found");
            echo json_encode($err);
          }else{
            echo json_encode($owners);
          }
        }
      }else{
        $err = generateError(405, $this->method . ' Method Not Allowed');
            echo json_encode($err);
      }
  }

  public function show(){
    $id = $_GET['id'];

    $api = new PetOwnerModel();

    $owner = $api->SearchByID($id);

    if(empty($owner)) {
      $err = generateError(404, "No Owners Found");
      echo json_encode($err);
    }else{
      echo json_encode($owner);
    }
  }

  public function add(){
    if($this->method == 'POST'){

        $api = new PetOwnerModel();
  
        $inputData = json_decode(file_get_contents("php://input"), true);
  
        $params = empty($inputData)? $_POST: $inputData;
  
        $has_error = false;
  
        $required = array('name', 'email', 'password');
  
        foreach($required as $requirement){
          if(!isset($params[$requirement])){
            $has_error = true;
            $err = generateError(422,"Missing $requirement");
            echo json_encode($err);
            break;
          }
        }
  
        if(!$has_error){
          $owner = $api->addOwner($params);
          echo json_encode($owner);
        }
      }else{
        $err = generateError(405, $this->method . ' Method Not Allowed');
            echo json_encode($err);
      }  
  }

  public function delete(){
    if($this->method == 'DELETE'){
        if (isset($_GET['id'])) {
          
          $api = new PetOwnerModel();
    
          $owner = $api->deletePetOwner($_GET['id']);
    
          if(empty($owner)){
            $err = generateError(404, "No Owner Found");
  
            echo json_encode($err);
          }else{
            echo json_encode($owner);
          }
        }else{
          $err = generateError(422, "Missing ID");
          echo json_encode($err);
        }
      }else{
        $err = generateError(405, $this->method . " Method Not Allowed");
            echo json_encode($err);
      }
  }

}